<div class="heading add">
	<h2>Product Add / <a href="/product/list">Product List</a></h2>
	<form method="post" id="add" action="/product/add">
		<button class="btn btn-secondary" name="save">Save</button>
		<input type="hidden" name="attr" id="attr" value="">
	</form>
</div>
<div class="container-fluid">
	<hr>
	<div class="row">
		<div class="col-md-2">
			<div class="form-group row">
				<label class="col-form-label col-md-2" for="sku">SKU</label>
				<div class="col-md-10">
					<input type="text" id="sku" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-form-label col-md-2" for="name">Name</label>
				<div class="col-md-10">
					<input type="text" id="name" class="form-control">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-form-label col-md-2" for="price">Price</label>
				<div class="col-md-10">
					<input type="text" id="price" class="form-control">
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group row">
				<label class="col-form-label col-md-4" for="type">Type Switcher</label>
				<div class="col-md-8">
					<select class="custom-select" id="type">
						<option value="0" selected>Type Switcher</option>
						<?php foreach ($types as $type): ?>
							<option value="<?php echo $type['type_id'] ?>"><?php echo $type['type_name']; ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="type-content"></div>
		</div>
	</div>	
</div>