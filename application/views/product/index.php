<div class="heading delete">
	<h2>Product List / <a href="/product/add">Product Add</a></h2>
	<form method="post" class="form-group" id="delete" action="/product/list">
		<select name="sku" class="custom-select" id="actions">
			<option>Mass Delete Action</option>
		</select>
		<button class="btn btn-secondary">Apply</button>
	</form>
</div>
<div class="container-fluid">
	<hr>
	<div class="row">
		<?php foreach ($products as $product): ?>
		<div class="col-md-3" id="<?php echo $product['sku']; ?>">
			<div class="card form-check">
				<input type="checkbox" class="form-check-input" value="<?php echo $product['sku']; ?>">
				<div class="card-body text-center">
					<?php echo $product['sku']; ?>
					<br>
					<?php echo $product['name']; ?>
					<br>
					<?php echo number_format($product['price'], 2, '.', ''); ?> $
					<br>
					<?php 
						switch ($product['type']) {
							case '1':
								echo "Size: " . $product['mb'] . " Mb";
								break;
							case '2':
								echo "Weight: " . $product['kg'] . " Kg";
								break;
							case '3':
								echo "Dimension: " . $product['height'] . "x" . $product['width'] . "x" . $product['length'];
								break;
						}
					?>
				</div>
			</div>
		</div>	
		<?php endforeach ?>
	</div>
</div>
<div class="results"></div>