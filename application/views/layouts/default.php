<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="/public/style/bootstrap.min.css">
	<link rel="stylesheet" href="/public/style/main.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.1/dist/sweetalert2.min.css">
	<script src="/public/scripts/jquery.js"></script>
	<script src="/public/scripts/form.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.13.1/dist/sweetalert2.all.min.js"></script>
</head>
<body>
	<?php echo $content; ?>
</body>
</html>