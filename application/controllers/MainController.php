<?php

namespace application\controllers;

use application\core\Controller;
use application\models\Main;

class MainController extends Controller {

	public function index() {
		$this->view->render('Home');
	}

}