<?php

namespace application\controllers;

use application\core\Controller;

class ProductController extends Controller
{
	
	public function index()
	{
		// If isset POST variable then get checked out product ID to delete it from DB and after that relocate back to product list
		if (isset($_POST['sku']))
		{
			if ($this->model->deleteProduct($_POST['sku']))
			{
				$this->view->redirect('/product/list');
			}
		}
		// Get all products from DB
		$result = $this->model->getProductList();
		$vars = [
			'products' => $result,
		];
		$this->view->render('Product List', $vars);
	}

	public function add()
	{
		// If isset POST variable then collect in array all inputs from form, then send it all to functions for DB query process
		if (isset($_POST['save']))
		{
			$productData = [
				'sku' => $_POST['sku'],
				'name' => $_POST['name'],
				'price' => $_POST['price'],
				'type' => $_POST['type'],
			];
			$attrData = [
				'product_id' => $_POST['sku'],
				'mb' => isset($_POST['size']) ? (int) $_POST['size'] : 'NULL',
				'kg' => isset($_POST['weight']) ? (int) $_POST['weight'] : 'NULL',
				'height' => isset($_POST['height']) ? (float) $_POST['height'] : 'NULL',
				'width' => isset($_POST['width']) ? (float) $_POST['width'] : 'NULL',
				'length' => isset($_POST['length']) ? (float) $_POST['length'] : 'NULL'
			];
			if ($this->model->addProduct($productData) && $this->model->addProductAttr($attrData))
			{
				$this->view->redirect('/product/list');
			}
		}
		// Get all product types from DB
		$result = $this->model->getTypeList();
		$vars = [
			'types' => $result,
		];
		$this->view->render('Product Add', $vars);
	}

}