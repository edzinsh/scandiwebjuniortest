<?php

namespace application\lib;

use PDO;

class Db {

	protected $db;
	protected static $instance;
	public static $countSql = 0;
	public static $queries = [];
	
	public function __construct() {
		$config = require 'application/config/db.php';
		$options = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
		];
		$this->db = new PDO('mysql:host='.$config['host'].';dbname='.$config['name'].'', $config['user'], $config['password'], $options);
	}

	public static function instance() {
		if (self::$instance === null) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function prepare($sql) {
		return $this->db->prepare($sql);
	}

	public function execute($sql, $params = []) {
		self::$countSql++;
		self::$queries[] = $sql;
		$stmt = $this->prepare($sql);
		return $stmt->execute($params);
	}

	public function query($sql, $params = []) {
		self::$countSql++;
		self::$queries[] = $sql;
		$stmt = $this->prepare($sql);
		$res = $stmt->execute($params);
		if ($res !== false) {
			return $stmt->fetchAll();
		}
		return [];
	}

}