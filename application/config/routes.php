<?php

return [

	'' => [
		'controller' => 'main',
		'action' => 'index'
	],

	'product/list' => [
		'controller' => 'product',
		'action' => 'index'
	],

	'product/add' => [
		'controller' => 'product',
		'action' => 'add'
	]
	
];