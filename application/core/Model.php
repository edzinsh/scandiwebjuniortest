<?php

namespace application\core;

use application\lib\Db;

abstract class Model {

	protected $db;
	protected $table;
	protected $pk = 'sku';

	// Creating database variable
	public function __construct() {
		$this->db = Db::instance();
	}

	public function findAll($table) {
		$table = $table ?: $this->table;
		$sql = "SELECT * FROM `{$table}`";
		return $this->db->query($sql);
	}

	public function findOne($id, $field = '') {
		$field = $field ?: $this->pk;
		$sql = "SELECT * FROM `{$this->table}` WHERE `$field` = ? LIMIT 1";
		return $this->db->query($sql, [$id]);
	}

	public function findBySql($sql, $params = []) {
		return $this->db->query($sql, $params);
	}

	public function deleteData($data, $table, $field = '') {
		$table = $table ?: $this->table;
		$field = $field ?: $this->pk;
		$sql = "DELETE FROM `{$table}` WHERE `$field` = ?";
		return $this->db->execute($sql, [$data]);
	}

	public function insertData($table, $columns) {
		$table = $table ?: $this->table;
		$columnString = implode(',', array_keys($columns));
		$valueString = implode(',', array_fill(0, count($columns), '?'));
		$sth = $this->db->prepare("INSERT INTO `{$table}`(" . $columnString . ") VALUES(" . $valueString . ")");
		var_dump($sth);
		return $sth->execute(array_values($columns));
	}

}