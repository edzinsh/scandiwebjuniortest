<?php

namespace application\models;

use application\core\Model;

class Product extends Model {

	public $table = 'products';
	public $pk = 'name';

	public function getProductList() {
		$result = $this->findBySql("SELECT * FROM {$this->table}
								  LEFT JOIN `product_types` ON `products`.`type` = `product_types`.`type_id`
								  LEFT JOIN `product_attr` ON `products`.`sku` = `product_attr`.`product_id`");
		return $result;
	}
	
	public function deleteProduct($data) {
		$result = $this->deleteData($data, $this->table, 'sku');
		$deleteAttrs = $this->deleteData($data, 'product_attr', 'product_id');
		
		return $result;
	}

	public function addProduct($data) {
		$result = $this->insertData($this->table, $data);
		return $result;
	}

	public function addProductAttr($data) {
		$result = $this->insertData('product_attr', $data);
		return $result;
	}

	public function getTypeList() {
		$result = $this->findAll('product_types');
		return $result;
	}

}