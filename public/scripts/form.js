$(document).ready(function() {
	// Get into PHP variable checked checkbox value (SKU) to delete record from Database
	$('#delete').submit(function() {
		$('#delete select option').val($('.form-check input[type="checkbox"]:checked').val());
	});
	// Change type content to set type with attributes for record
	$('#type').change(function(){
		if (this.value == 0)
		{
			$('.type-content .form-group').remove();
			$('.type-content .description').remove();
		}
		else if (this.value == 1)
		{
			$('.type-content').html('<div class="form-group row"><label class="col-form-label col-md-2" for="size">Size</label><div class="col-md-10"><input type="number" id="size" class="form-control"></div></div><p class="description"></p>');
			$('.type-content .description').append('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed explicabo quia adipisci ratione qui suscipit dignissimos aliquid deleniti ex assumenda asperiores placeat hic, beatae delectus reprehenderit! Iste natus, pariatur aliquid!');
		}
		else if (this.value == 2)
		{
			$('.type-content').html('<div class="form-group row"><label class="col-form-label col-md-2" for="size">Weight</label><div class="col-md-10"><input type="number" id="weight" class="form-control"></div></div><p class="description"></p>');
			$('.type-content .description').append('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed explicabo quia adipisci ratione qui suscipit dignissimos aliquid deleniti ex assumenda asperiores placeat hic, beatae delectus reprehenderit! Iste natus, pariatur aliquid!');
		}
		else if (this.value == 3)
		{
			$('.type-content').html('<div class="form-group row"><label class="col-form-label col-md-2" for="width">Width</label><div class="col-md-10"><input type="number" id="width" class="form-control"></div></div><div class="form-group row"><label class="col-form-label col-md-2" for="height">Height</label><div class="col-md-10"><input type="number" id="height" class="form-control"></div></div><div class="form-group row"><label class="col-form-label col-md-2" for="length">Length</label><div class="col-md-10"><input type="number" id="length" class="form-control"></div></div><p class="description"></p>');
			$('.type-content .description').append('Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed explicabo quia adipisci ratione qui suscipit dignissimos aliquid deleniti ex assumenda asperiores placeat hic, beatae delectus reprehenderit! Iste natus, pariatur aliquid!');
		}
	});
	// Get into PHP variables all of attributes with 
	$('#add').submit(function(){
		var sku = $('#sku').val();
		var name = $('#name').val();
		var price = $('#price').val();
		if (!sku || !name || !price)
		{
			Swal.fire({
				type: 'error',
				title: 'Oops...',
				text: 'Something went wrong!'
			});
			return false;
		}
		$('#add').append('<input type="hidden" id="sku" name="sku" value="'+sku+'">');
		$('#add').append('<input type="hidden" id="name" name="name" value="'+name+'">');
		$('#add').append('<input type="hidden" id="price" name="price" value="'+price+'">');
		var type = $('#type').val();
		if (type == 1)
		{
			var size = $('#size').val();
			$('#add').append('<input type="hidden" id="size" name="size" value="'+size+'">');
			$('#add').append('<input type="hidden" id="type" name="type" value="'+type+'">');
		}
		else if (type == 2)
		{
			var weight = $('#weight').val();
			$('#add').append('<input type="hidden" id="weight" name="weight" value="'+weight+'">');
			$('#add').append('<input type="hidden" id="type" name="type" value="'+type+'">');
		}
		else if (type == 3)
		{
			var height = $('#height').val();
			var width = $('#width').val();
			var length = $('#length').val();
			if (!height || !width || !length)
			{
				Swal.fire({
					type: 'error',
					title: 'Oops...',
					text: 'Something went wrong!'
				});
				return false;
			}
			$('#add').append('<input type="hidden" id="height" name="height" value="'+height+'">');
			$('#add').append('<input type="hidden" id="width" name="width" value="'+width+'">');
			$('#add').append('<input type="hidden" id="length" name="length" value="'+length+'">');
			$('#add').append('<input type="hidden" id="type" name="type" value="'+type+'">');
		} else {
			Swal.fire({
				type: 'error',
				title: 'Oops...',
				text: 'Something went wrong!'
			});
			return false;
		}
		Swal.fire({
			type: 'success',
			title: '=)',
			text: 'Record successfuly saved',
			showConfirmButton: false,
			timer: 2000
		});
		setTimeout(function(){
			window.location.href = "/product/list";
		}, 2000);
	});
});